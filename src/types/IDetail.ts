import { IImage } from "./IImage"
import { INetwork } from "./INetwork"
import { IAverage } from "./IAverage"

export interface IDetail {
	id: number,
	ended?: string,
	genres: string[],
	image?: IImage,
	name: string,
	rating: IAverage,
	summary: string,
	network?: INetwork
	premiered: string
}