import { IImage } from "./IImage"
import { INetwork } from "./INetwork"
import { IAverage } from "./IAverage"
import { IDetail } from "./IDetail"

export interface IMain {
    score: number,
    show: IDetail
}

