import { ICountry } from "./ICountry"

export interface INetwork {
    id: number,
	name: string,
	country: ICountry,
	officialSite: string,
}