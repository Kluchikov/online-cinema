import { applyMiddleware } from "@reduxjs/toolkit"
import { composeWithDevTools } from "redux-devtools-extension"
import {legacy_createStore as createStore, combineReducers} from 'redux'

import CategoryFilmsReducer from "./CategoryFilms/reducer"
import MainFilmsReducer from "./MainFilms/reducer"
import DetailFilmReducer from "./DetailFilms/reducer"

import thunk from "redux-thunk"

const rootReducer = combineReducers({
    DetailFilmReducer,
    CategoryFilmsReducer,
    MainFilmsReducer,
})

 
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store
