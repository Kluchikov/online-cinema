import { IStore } from "./types"

export const DetailSelectList = (state : { DetailFilmReducer : IStore}) : IStore['list'] => state.DetailFilmReducer.list
