import { Dispatch } from "redux"
import getDetailFilms from "../../serveces/getDetailFilms"

import { IStore } from "./types"

export const setDetail = (list : IStore['list']) => {
	return {
		type: 'show/setDetail',
		payload: list,
	}
}

export const loadDetail = (value: string | undefined) => async (dispatch : Dispatch) => {
	try {
		let responseArray = []
		const response = await getDetailFilms(value)
		responseArray[0] = response.data
		dispatch(setDetail(responseArray))
	} catch(e) {
		console.log('Произошла: ', e)
	}
}
