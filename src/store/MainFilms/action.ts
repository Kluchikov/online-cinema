import { Dispatch } from "redux"
import getMainFilms from "../../serveces/getMainFilms"
import { IStore } from "./types"


export const setFilmsAction = (list: IStore['list']) => {
	return {
		type: 'MainFilms/setMainFilms',
		mainLoad: list,
	}
}

export const loadMainFilms = () => async (dispath: Dispatch) => {
	try {
		const response = await getMainFilms()
		dispath(setFilmsAction(response.data))

	} catch (e) {
		console.log(e, 'произошла ошибка')
	}
}