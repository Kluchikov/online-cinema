import { IDetail } from "../../types/IDetail"

export interface IStore {
    list: IDetail[]
}