import { IStore } from "./types"

export const CategorySelectList = (state: {CategoryFilmsReducer: IStore}): IStore['list'] => state.CategoryFilmsReducer.list