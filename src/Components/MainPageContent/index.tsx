import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { loadMainFilms } from "../../store/MainFilms/action";
import { MainSelectList } from "../../store/MainFilms/selectors";

import { NavLink } from "react-router-dom";

import routeFilmDetailPage from "../../Pages/FilmDetailPage/routes";
import prepareDate from "../../utils/prepareDate";
import prepareCountry from "../../utils/prepareCountry";
import prepareImage from "../../utils/prepareImage";

import './style.scss'

const MainPageContent = () => {
    const dispatch = useDispatch()
	const MainFilmsList = useSelector(MainSelectList)

	useEffect(() => {
		dispatch(loadMainFilms())
	}, [dispatch])

    return (
        <div className="container">
            <div className="main-head">
                <p className="main-logo">MOVIESInfo</p>
                <p className="main-title">Самый популярный портал о фильмах</p>
            </div>
            <div className="row main-page">
                {MainFilmsList.slice(0,8).map((item) => (
                    <NavLink className="film col-3" key={item.show.id} to={routeFilmDetailPage(item.show.id.toString())}>
                        {prepareImage(item.show.image?.medium)}
                        <div className="film-title">
                            <p className="name">{item.show.name}</p>
                            <p className="premiered">{prepareDate(item.show.premiered)}({prepareCountry(item.show.network?.country.name)})</p>
                            <p className="genres">{item.show.genres.join(', ')}</p>
                        </div>
                    </NavLink>
                ))}
            </div>
        </div>
    )
}

export default MainPageContent