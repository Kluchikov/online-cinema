import React from "react";

import { Route, Switch, Redirect } from "react-router-dom";

import MainPage from "../../Pages/MainPage";
import CategoryPage from "../../Pages/СategoryPage";
import InfoPage from "../../Pages/InfoPage";
import FilmDetailPage from "../../Pages/FilmDetailPage";
import Header from "../Header";
import Footer from "../Footer";

import routeFilmDetailPage from "../../Pages/FilmDetailPage/routes";
import routeInfoPage from "../../Pages/InfoPage/routes";
import routeMainPage from "../../Pages/MainPage/roures";
import routeCategoryPage from "../../Pages/СategoryPage/routes";

import './style.scss'

const AppContent = () => {
	return (
		<div className="main-wrapp">
			<Header />
			<main>
				<Switch>
					<Route exact path={routeInfoPage()} component={InfoPage} />
					<Route exact path={routeMainPage()} component={MainPage} />
					<Route exact path={routeCategoryPage()} component={CategoryPage} />
					<Route exact path={routeFilmDetailPage()} component={FilmDetailPage} />
					<Redirect to={routeMainPage()} />
				</Switch>
			</main>
			<Footer />
		</div>
	)
}

export default AppContent