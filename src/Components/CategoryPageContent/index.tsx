import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { loadCategoryFilms } from "../../store/CategoryFilms/actions";
import { CategorySelectList } from "../../store/CategoryFilms/selectors";

import routeFilmDetailPage from "../../Pages/FilmDetailPage/routes";

import { NavLink } from "react-router-dom";

import './style.scss'

const CategoryPageContent = () => {
    
    const dispatch = useDispatch()
	const CategoryFilmsList = useSelector(CategorySelectList)

	useEffect(() => {
		dispatch(loadCategoryFilms())
	}, [dispatch])

    return (
        <div className="container">
            <div className="category row">
                <div><h2>Выбранная категория: <span className="chooseGenres">Animals</span></h2></div>
                {CategoryFilmsList.map((item) => (
                    <NavLink key={item.id} className="category-main col-6" to={routeFilmDetailPage(item.id.toString())}>
                        <div className="category-image">
                            <img className="film-image" src={item.image?.medium} alt='show'/>
                        </div>
                        <div className="category-text">
                            <p>{item.name}</p>
                            <p>{item.genres.join(', ')}</p>
                        </div>
                    </NavLink>
                ))}
            </div>
        </div>
    )
}

export default CategoryPageContent
