import React from "react";

import { NavLink } from "react-router-dom";

import routeInfoPage from "../../Pages/InfoPage/routes";
import routeMainPage from "../../Pages/MainPage/roures";
import routeCategoryPage from "../../Pages/СategoryPage/routes";

import './style.scss'

const Header = () => {
	return (
		<header className="main-header">
			<NavLink className="title" to={routeMainPage()}>MOVESInfo</NavLink>
			<nav>
				<NavLink to={routeMainPage()} activeClassName={'linkActive'}>
					Главная
				</NavLink>
				<NavLink to={routeCategoryPage()} activeClassName={'linkActive'}>
					Фильмы по категориям
				</NavLink>
				<NavLink to={routeInfoPage()} activeClassName={'linkActive'}>
					О нас
				</NavLink>
			</nav>
		</header>
	)
}

export default Header