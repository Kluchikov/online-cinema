import axios, {AxiosResponse} from "axios";

const getDetailFilms = (value?: string | undefined): Promise<AxiosResponse> => axios.get('https://api.tvmaze.com/shows/' + value)

export default getDetailFilms