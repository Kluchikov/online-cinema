const prepareDate = (value: string | undefined) => value ? value.slice(0, 4) : 'N/A'

export default prepareDate