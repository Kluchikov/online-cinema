const prepareCountry = (value: string | undefined) => value ? value : 'N/A'

export default prepareCountry