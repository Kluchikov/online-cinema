const prepareSummary = (value: string | undefined) => value ? value.replace( /(<([^>]+)>)/ig, ''): 'N/A'

export default prepareSummary